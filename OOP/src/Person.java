
public class Person {
	
	private String name;
	private static int anzahl;
	
	public Person(String name) {
	//private this.name = name;
		setName(name);
		anzahl++;
	}
	
	public String getName() {
		return name;
	}
	
	public static int getAnzahl() {
		return anzahl;
		
	}
		
	public void setName(String name) {
		if (name != null)
			this.name = name;
		
	}
}
