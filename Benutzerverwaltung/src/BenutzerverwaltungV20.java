import java.io.InputStream;
import java.util.Arrays;
import java.util.Scanner;
import java.io.Console;

public class Benutzerverwaltung {

    public static void main(String[] args) {

        BenutzerverwaltungV10.start();

    }
}

class BenutzerverwaltungV10{
	
    public static void start(){
    	
        BenutzerListe benutzerListe = new BenutzerListe();

        benutzerListe.insert(new Benutzer("Paula", Crypto.encrypt("paula".toCharArray())));
        benutzerListe.insert(new Benutzer("Adam37", Crypto.encrypt("adam37".toCharArray())));
        benutzerListe.insert(new Benutzer("Darko", Crypto.encrypt("darko".toCharArray())));
        benutzerListe.insert(new Benutzer("Admin", new char[]{36, 61, 72, 72, 75, 13, 14, 15}));

        int menuAuswahl = optionAuswaehlen();
        
        if(menuAuswahl == 1) {
        	
        	anmelden(benutzerListe);
        	
        } 
        
        else if(menuAuswahl == 2) {
        	
        	registrieren(benutzerListe);
        	
        }  
        
        // Hier bitte das Men� mit der Auswahl
        //  - Anmelden
        //  - Registrieren
        // einf�gen, sowie die entsprechenden Abl�ufe:
        // Beim Registrieren 2x das Passwort einlesen und vergleichen,
        // das neue Benutzerobjekt erzeugen und in die Liste einf�gen.
        // Beim Anmelden (max. 3 Versuche) name und passwort einlesen,
        // in der Liste nach dem Namen suchen und das eingegebene Passwort
        // mit dem gespeicherten vergleichen.

        System.out.println(benutzerListe.select());
        System.out.println(benutzerListe.select("Peter"));
        System.out.println(benutzerListe.select("Darko"));
    }
    
    public static boolean authenticate(String name, char[] cryptoPw) {
    	Benutzer b = BenutzerListe.getBenutzer(name);
    	if(b != null) {
    		if(b.hasPasswort(cryptoPw)){
    			return true;
    		}
    	}
    	return false;
    }
    
    public static int optionAuswaehlen() {
    	
        int auswahl = 0;

        while (auswahl != 1 && auswahl != 2) {
        	
            Scanner tastatur = new Scanner(System.in);
            
            System.out.println("Option ausw�hlen:");
            System.out.println("-------------------------\n");
            System.out.println("1 - Anmelden");
            System.out.println("2 - Registrieren");
            
            auswahl = tastatur.nextInt();
        }

        return auswahl;   
        
    }
    
    public static void anmelden(BenutzerListe benutzerListe ) {
    	
    	int i = 0;
    	
    	Scanner tastatur = new Scanner(System.in);
    	
    	System.out.println("Bitte Benutzername eingeben");
    	
    	do {
    		String benutzerName = tastatur.next();
        	System.out.println("Bitte Passwort eingeben");
        	String passwort = tastatur.next();
        	
        	if (!benutzerListe.select(benutzerName).equals("")) {
        		String username = benutzerListe.select(benutzerName);
        		String[] userArray = username.split(" ");
        		
        		if (passwort.equals(userArray[1])) {
        			System.out.println("Login erfolgreich");
        			return;
        		}
        		
        	}
        	
        	i++; 
        	
        	System.out.println("Benutzername und/oder Passwort sind/ist falsch. Bitte versuchen Sie es erneut");
        	
    	} 
    	
    	while (i < 3);
    	
    	
    }
    
    public static void registrieren(BenutzerListe benutzerListe) {
    	
    	Scanner tastatur = new Scanner(System.in);
    	
    	System.out.println("Bitte den gew�nschten Benutzernamen eingeben:");
    	
    	String benutzerName = tastatur.next();
    	
    	if (benutzerListe.select(benutzerName).equals("")) {
    		
    		System.out.println("Bitte das gew�nschtes Passwort eingeben:");
    		
    		String passwort = tastatur.next();
    		
    		System.out.println("Bitte das gew�nschtes Passwort erneut eingeben:");
    		
    		if (tastatur.next().equals(passwort)){
    			
    			benutzerListe.insert(new Benutzer(benutzerName, passwort));
    			
    			System.out.println("Benutzer angelegt!");
    			
    		}
    		
    	}
    }
}

class BenutzerListe{
    private Benutzer first;
    private Benutzer last;
    public BenutzerListe(){
        first = last = null;
    }
    public static Benutzer getBenutzer(String name) {
		// TODO Auto-generated method stub
		return null;
	}
	public void insert(Benutzer b){
        // Sicherheitshalber setzen wir
        // den Nachfolger auf null:
        b.setNext(null);
        if(first == null){
            first = last = b;
        }
        else{
            last.setNext(b);
            last = b;
        }
    }
    public String select(){
        String s = "";
        Benutzer b = first;
        while(b != null){
            s += b.toString() + '\n';
            b = b.getNext();
        }
        return s;
    }
    public String select(String name){
        Benutzer b = first;
        while(b != null){
            if(b.hasName(name)){
                return b.toString();
            }
            b = b.getNext();
        }
        return "";
    }

    public void delete(String name){
    	Benutzer delUser = first;
        
        while (delUser != null) {
       	 if(delUser.getName().equals(name)) {
       		 if (delUser == first) {
       			 first = first.getNext();
       			 delUser.setNext(null);
       		 } else {
       			 Benutzer preUser = first;
       			 
       			 while (preUser != null && preUser.getNext() != null) {
           			 if (preUser.getNext().getName().equals(name)) {
           				 if (delUser.getNext() != null) {
           					preUser.setNext(delUser.getNext());
           					delUser.setNext(null);
           				 } else {
           					preUser.setNext(null);
           				 }
           			 }
           			preUser = preUser.getNext();
       			 }
       		 }
       	 }
       	delUser = delUser.getNext();
        }
    }
    }

class Benutzer{
    private String name;
    private char[] passwort;  
    private Benutzer next;

    public Benutzer(String name, char[] pw){
        this.name = name;
        this.passwort = pw;

        this.next = null;
    }

    public String getName() {
		return name;
	}

	public boolean hasPasswort(char[] cryptoPw){
        return Arrays.equals(this.passwort, cryptoPw);
     
	}

	public boolean hasName(String name){
        return name.equals(this.name);
    }

    public String toString(){
        String s = "";
        s += name + " ";
        s += passwort;
        return s;
    }

    public Benutzer getNext(){
        return next;
    }

    public void setNext(Benutzer b){
        next = b;
    }
    
  static class Crypto {
        private static int cryptoKey = 65500;

        public static char[] encrypt(char[] s) {
            char[] encrypted = new char[s.length];
            for(int i = 0; i < s.length; i++) {
                encrypted[i] = (char)((s[i] + cryptoKey) % 128);
            }
            return encrypted;
        }
    }
}


