import java.util.Scanner;

class BenutzerverwaltungV10{
	
    public static void start(){
    	
        BenutzerListe benutzerListe = new BenutzerListe();

        benutzerListe.insert(new Benutzer("Paula", "paula"));
        benutzerListe.insert(new Benutzer("Adam37", "adam37"));
        benutzerListe.insert(new Benutzer("Darko", "darko"));

        int menuAuswahl = optionAuswaehlen();
        
        if(menuAuswahl == 1) {
        	
        	anmelden(benutzerListe);
        	
        } 
        
        else if(menuAuswahl == 2) {
        	
        	registrieren(benutzerListe);
        	
        }  
        
        // Hier bitte das Men� mit der Auswahl
        //  - Anmelden
        //  - Registrieren
        // einf�gen, sowie die entsprechenden Abl�ufe:
        // Beim Registrieren 2x das Passwort einlesen und vergleichen,
        // das neue Benutzerobjekt erzeugen und in die Liste einf�gen.
        // Beim Anmelden (max. 3 Versuche) name und passwort einlesen,
        // in der Liste nach dem Namen suchen und das eingegebene Passwort
        // mit dem gespeicherten vergleichen.

        System.out.println(benutzerListe.select());
        System.out.println(benutzerListe.select("Peter"));
        System.out.println(benutzerListe.select("Darko"));
    }
    
    public static int optionAuswaehlen() {
    	
        int auswahl = 0;

        while (auswahl != 1 && auswahl != 2) {
        	
            Scanner tastatur = new Scanner(System.in);
            
            System.out.println("Option ausw�hlen:");
            System.out.println("-------------------------\n");
            System.out.println("1 - Anmelden");
            System.out.println("2 - Registrieren");
            
            auswahl = tastatur.nextInt();
        }

        return auswahl;   
        
    }
    
    public static void anmelden(BenutzerListe benutzerListe ) {
    	
    	int i = 0;
    	
    	Scanner tastatur = new Scanner(System.in);
    	
    	System.out.println("Bitte Benutzername eingeben");
    	
    	do {
    		String benutzerName = tastatur.next();
        	System.out.println("Bitte Passwort eingeben");
        	String passwort = tastatur.next();
        	
        	if (!benutzerListe.select(benutzerName).equals("")) {
        		String username = benutzerListe.select(benutzerName);
        		String[] userArray = username.split(" ");
        		
        		if (passwort.equals(userArray[1])) {
        			System.out.println("Login erfolgreich");
        			return;
        		}
        		
        	}
        	
        	i++; 
        	
        	System.out.println("Benutzername und/oder Passwort sind/ist falsch. Bitte versuchen Sie es erneut");
        	
    	} 
    	
    	while (i < 3);
    	
    	
    }
    
    public static void registrieren(BenutzerListe benutzerListe) {
    	
    	Scanner tastatur = new Scanner(System.in);
    	
    	System.out.println("Bitte den gew�nschten Benutzernamen eingeben:");
    	
    	String benutzerName = tastatur.next();
    	
    	if (benutzerListe.select(benutzerName).equals("")) {
    		
    		System.out.println("Bitte das gew�nschtes Passwort eingeben:");
    		
    		String passwort = tastatur.next();
    		
    		System.out.println("Bitte das gew�nschtes Passwort erneut eingeben:");
    		
    		if (tastatur.next().equals(passwort)){
    			
    			benutzerListe.insert(new Benutzer(benutzerName, passwort));
    			
    			System.out.println("Benutzer angelegt!");
    			
    		}
    		
    	}
    }
}
