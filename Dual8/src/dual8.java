import java.util.Scanner;

public class dual8 {

    public static void main(String[] args) {

        Scanner myScanner = new Scanner(System.in);

        System.out.print("Geben Sie eine 8-stellige Dualzahl ein: ");
        String ziffernfolge = myScanner.next();
        char ziffer7 = ziffernfolge.charAt(0);
        char ziffer6 = ziffernfolge.charAt(1);
        char ziffer5 = ziffernfolge.charAt(2);
        char ziffer4 = ziffernfolge.charAt(3);
        char ziffer3 = ziffernfolge.charAt(4);
        char ziffer2 = ziffernfolge.charAt(5);
        char ziffer1 = ziffernfolge.charAt(6);
        char ziffer0 = ziffernfolge.charAt(7);

		System.out.println("Sie haben diese 8 Ziffern eingegeben: " +
                ziffer7 + ziffer6 + ziffer5 + ziffer4 + ziffer3 + ziffer2 + ziffer1 + ziffer0);

        byte bit7 = (byte)(ziffer7 - '0');
        byte bit6 = (byte)(ziffer6 - '0');
        byte bit5 = (byte)(ziffer5 - '0');
        byte bit4 = (byte)(ziffer4 - '0');
        byte bit3 = (byte)(ziffer3 - '0');
        byte bit2 = (byte)(ziffer2 - '0');
        byte bit1 = (byte)(ziffer1 - '0');
        byte bit0 = (byte)(ziffer0 - '0');

        // int verwenden wir hier nur für die einfache Ausgabe mit println,
        // weil Java keine unsigned-Datentypen kennt.
        // Für die interne Weiterverarbeitung z.B. als Oktett einer IPv4-Adresse
        // könnten wir byte oktett verwenden.
        int zahlenwert = (bit7 << 7) | (bit6 << 6) | (bit5 << 5) | (bit4 << 4) |
                         (bit3 << 3) | (bit2 << 2) | (bit1 << 1) | (bit0 << 0);

        System.out.println("Dezimaldarstellung = " + zahlenwert);
    }
}