public class Aufgabe02 {

	public static void main(String[] args) {

		String a = "!";
		String b = "*";
		String c = "=";

		System.out.printf( "%d%-3s%-20s%s%4d\n", 0, a, c, c, 1 );

		System.out.printf( "%d%-3s%-2s%-18d%s%4d\n", 1, a, c, 1 , c, 1 );

		System.out.printf( "%d%-3s%-2s%-2d%-2s%-14d%s%4d\n", 2, a, c, 1, b ,2 , c, 2 );

		System.out.printf( "%d%-3s%-2s%-2d%-2s%-2d%-2s%-10d%s%4d\n", 3, a, c, 1, b ,2, b, 3 , c, 6 );

		System.out.printf( "%d%-3s%-2s%-2d%-2s%-2d%-2s%-2d%-2s%-6d%s%4d\n", 4, a, c, 1, b ,2, b, 3, b, 4 , c, 24 );

		System.out.printf( "%d%-3s%-2s%-2d%-2s%-2d%-2s%-2d%-2s%-2d%-2s%-2d%s%4d\n", 5, a, c, 1, b ,2, b, 3, b, 4 ,b, 5, c, 120 );


}
}