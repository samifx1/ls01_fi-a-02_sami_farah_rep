import java.util.Scanner;

public class Kopfgesteuertenschleifen {

	
	public static void main(String[] args) {
		countDownWhile();
		countDownFor();
		countUpWhile();
		countUpFor();
	}
	
	public static void countDownWhile() {
		Scanner zahleingeben = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie ein Zahl ein: ");
		int nZahl = zahleingeben.nextInt();
		
		while(nZahl > 0){
		System.out.println(nZahl);
		nZahl--;
		// nZahl = nZahl - 1
		
		try {
	  			Thread.sleep(250);
	  		} catch (InterruptedException e) {
	  			// TODO Auto-generated catch block
	  			e.printStackTrace();
	  		}
		}
	}
	
	public static void countDownFor() {
		Scanner zahleingeben = new Scanner(System.in);
		System.out.print("Bitte geben Sie ein Zahl ein: ");
		int nZahl = zahleingeben.nextInt();
		for(; nZahl > 0 ; nZahl--) {
			System.out.println(nZahl);
			
			try {
	  			Thread.sleep(250);
	  		} catch (InterruptedException e) {
	  			// TODO Auto-generated catch block
	  			e.printStackTrace();
	  		}
	
		}
	}
	
	
	
	public static void countUpWhile() {
		Scanner zahleingeben = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie ein Zahl ein: ");
		int nZahl = zahleingeben.nextInt();
		int nZahl1 = 1;
		while(nZahl1 <= nZahl){
		System.out.println(nZahl1);
		nZahl1++;
		// nZahl1 = nZahl1 + 1
		
		try {
	  			Thread.sleep(250);
	  		} catch (InterruptedException e) {
	  			// TODO Auto-generated catch block
	  			e.printStackTrace();
	  		}
		}
	} 
	
	public static void countUpFor() {
		Scanner zahleingeben = new Scanner(System.in);
		System.out.print("Bitte geben Sie ein Zahl ein: ");
		int nZahl = zahleingeben.nextInt();
		for(int nZahl1 = 1; nZahl1 <= nZahl  ; nZahl1++) {
			System.out.println(nZahl1);
	
		try {
	  			Thread.sleep(250);
	  		} catch (InterruptedException e) {
	  			// TODO Auto-generated catch block
	  			e.printStackTrace();
	  		}
		}
	}
	

}