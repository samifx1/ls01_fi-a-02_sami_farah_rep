import java.util.Scanner;

class array
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag;
       double r�ckgabebetrag;       
       char weiterKaufen = 'j';
       
       do  {
    		   
       zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
       
       // Geldeinwurf
       // -----------
       
       r�ckgabebetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag);

       // Fahrscheinausgabe
       // -----------------
      
       fahrkartenAusgeben();

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
      
       rueckgeldAusgeben(r�ckgabebetrag);
       
       // weitere Tickets kaufen?
       
       System.out.print("Noch einen Kaufvertrag t�tigen? [j/n] ");
       weiterKaufen = tastatur.next().charAt(0);
       } while(weiterKaufen == 'j');
    
    }
    
    private static double fahrkartenbestellungErfassen(Scanner eingabe) {
        
		//verschieden Fahkarten in den Array festlegen
		
		String[] fahrkartenBezeichnung = new String[10];
			fahrkartenBezeichnung[0] =  "Einzelfahrschein Berlin AB";
			fahrkartenBezeichnung[1] =  "Einzelfahrschein Berlin BC";        
			fahrkartenBezeichnung[2] =  "Einzelfahrschein Berlin ABC";
			fahrkartenBezeichnung[3] =  "Kurzstrecke";
			fahrkartenBezeichnung[4] =  "Tageskarte Berlin AB";
			fahrkartenBezeichnung[5] =  "Tageskarte Berlin BC";        
			fahrkartenBezeichnung[6] =  "Tageskarte Berlin ABC";        
			fahrkartenBezeichnung[7] =  "Kleingruppen-Tageskarte Berlin AB";
			fahrkartenBezeichnung[8] =  "Kleingruppen-Tageskarte Berlin BC";
			fahrkartenBezeichnung[9] =  "Kleingruppen-Tageskarte Berlin ABC";        
        
		//die Preis der Fahrkarten festlegen
		
		double[] fahrkartenPreis = new double[10];
			fahrkartenPreis[0] = 2.90;
			fahrkartenPreis[1] = 3.30;
			fahrkartenPreis[2] = 3.60;
			fahrkartenPreis[3] = 1.90;
			fahrkartenPreis[4] = 8.60;
			fahrkartenPreis[5] = 9.00;
			fahrkartenPreis[6] = 9.60;
			fahrkartenPreis[7] = 23.50;
			fahrkartenPreis[8] = 24.30;
			fahrkartenPreis[9] = 24.90;
        
		System.out.println("W�hlen Sie Ihre W�nschkarte aus: ");
        
		for (int i=0; i < fahrkartenBezeichnung.length; i++) {
			int auswahl = i+1;
			System.out.println(fahrkartenBezeichnung[i] + "[" + fahrkartenPreis[i] + "EUR] (" + auswahl + ")");
		}
        
        System.out.print("\nIhre Wahl: ");
        
        int auswahlnummer = eingabe.nextInt();
        
        while (auswahlnummer <= 0 || auswahlnummer > 10) 
        {
        		
        		System.out.println("Sie haben einen ung�ltigen Zahl eingegeben. ");
        		System.out.print("\nIhre Wahl: ");
        		
        }
        
        System.out.println("Anzahl der Fahrkarten: ");
        
        int anzahlFahrkarten = eingabe.nextInt();
        
        if (anzahlFahrkarten > 10 && anzahlFahrkarten < 1 ) {

        	System.out.println("Sie d�rfen 1-10 Karten kaufen. \nBei einer Eingabe von ung�ltigen Wert, wird der Wert 1 genommen.");
        	
        	anzahlFahrkarten = 1;
        }
        
        
        return anzahlFahrkarten * fahrkartenPreis[auswahlnummer - 1];
        
	}

	private static double fahrkartenBezahlen(Scanner eingabe, double betrag){
    	double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < betrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f ", (betrag-eingezahlterGesamtbetrag  ));
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   double eingeworfeneM�nze = eingabe.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
        return eingezahlterGesamtbetrag - betrag;
    }
    
    private static void fahrkartenAusgeben() {
    	 System.out.println("\nFahrschein wird ausgegeben");
         for (int i = 0; i < 8; i++)
         {
            System.out.print("=");
            try {
  			Thread.sleep(250);
  		} catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
         }
         System.out.println("\n\n");
    }
    
    private static void rueckgeldAusgeben(double r�ckgabebetrag) {
    	 if(r�ckgabebetrag > 0.0)
         {
      	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f " + "EURO", r�ckgabebetrag);
      	   System.out.println(" wird in folgenden M�nzen ausgezahlt:");

             while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
             {
          	  System.out.println("2 EURO");
  	          r�ckgabebetrag -= 2.0;
             }
             while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
             {
          	  System.out.println("1 EURO");
  	          r�ckgabebetrag -= 1.0;
             }
             while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
             {
          	  System.out.println("50 CENT");
  	          r�ckgabebetrag -= 0.5;
             }
             while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
             {
          	  System.out.println("20 CENT");
   	          r�ckgabebetrag -= 0.2;
             }
             while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
             {
          	  System.out.println("10 CENT");
  	          r�ckgabebetrag -= 0.1;
             }
             while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
             {
          	  System.out.println("5 CENT");
   	          r�ckgabebetrag -= 0.05;
             }
         }

         System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                            "vor Fahrtantritt entwerten zu lassen!\n"+
                            "Wir w�nschen Ihnen eine gute Fahrt.");
    }
}